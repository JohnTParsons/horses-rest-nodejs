'use strict';

const lokiProvider = require('../database/lokiProvider');

/**
 * @swagger
 * /horses:
 *   get:
 *     tags:
 *       - Public
 *     summary: Get all horses
 *     operationId: getHorses
 *     responses:
 *       200:
 *         description: 0-to-many horses found
 *       400:
 *         description: Error finding horses
 */
exports.getHorses = async ctx => {
  const { Horse } = lokiProvider.getCollections();
  try {
    const data = await Horse.find({});
    ctx.res.ok(data, 'getHorses success');
  } catch (e) {
    ctx.res.badRequest(null, 'getHorses internal failure');
  }
};

/**
 * @swagger
 * /horses/:id:
 *   get:
 *     tags:
 *       - Public
 *     summary: Find a horse
 *     operationId: getHorses
 *     responses:
 *       200:
 *         description: Matching horse found
 *       404:
 *         description: No matching horse found
 *       400:
 *         description: Error finding horses
 */
exports.getHorse = async ctx => {
  const { Horse } = lokiProvider.getCollections();
  const id = parseInt(ctx.params.id);
  try {
    const data = await Horse.findOne({ id });
    if (!data)
      ctx.res.notFound(null, 'getHorse match failure');

    else
      ctx.res.ok(data, 'getHorse success');
  } catch (e) {
    ctx.res.badRequest(null, 'getHorse internal failure');
  }
};

/* /horses:
*   post:
*     tags:
*       - Public
*     summary: Create a horse
*     operationId: createHorse
*     body: a horse object
*     responses:
*       200:
*         description: Horse created
*       400:
*         description: Error creating horse
*/
exports.createHorse = async ctx => {
  const { Horse } = lokiProvider.getCollections();
  const { id, horseName, ownerName, handicap } = ctx.request.body;
  try {
    const data = await Horse.insert({
      id,
      horseName,
      ownerName,
      handicap
    });
    ctx.res.created(data, 'createHorse success');
  } catch (e) {
    ctx.res.badRequest(null, 'createHorse internal failure');
  }
};

/* /horses:
*   delete:
*     tags:
*       - Public
*     summary: Delete a horse
*     operationId: createHorse
*     responses:
*       200:
*         description: Horse deleted
*       400:
*         description: Error deleting horse
*/
exports.deleteHorse = async ctx => {
  const { Horse } = lokiProvider.getCollections();
  const id = parseInt(ctx.params.id);
  try {
    const data = await Horse.chain()
      .find({ id })
      .remove();
    ctx.res.ok(data, 'deleteHorse success');
  } catch (e) {
    ctx.res.badRequest(null, 'deleteHorse internal failure');
  }
};
