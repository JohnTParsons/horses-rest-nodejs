'use strict';

const Router = require('koa-router');
const homeController = require('./controllers/home');
const horseController = require('./controllers/horse');

const router = new Router();
router.get('/', homeController.welcome);
router.get('/spec', homeController.showSwaggerSpec);

router.get('/horses', horseController.getHorses);
router.get('/horses/:id', horseController.getHorse);
router.post('/horses', horseController.createHorse);
router.delete('/horses/:id', horseController.deleteHorse);

module.exports = router;
