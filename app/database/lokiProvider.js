'use strict';

const Loki = require('lokijs');

const collections = {};
const loadHandler = () => {
  // if database did not exist it will be empty so I will initialise here
  collections.Horse = db.getCollection('horse');
  if (!collections.Horse)
    collections.Horse = db.addCollection('horse');
};
const db = new Loki('loki.json', { autoload: true, autosave: true, autoloadCallback: loadHandler });

exports.getCollections = () => collections;
