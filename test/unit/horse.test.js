'use strict';

const horseController = require('../../app/controllers/horse');
const responseHandler = require('../../app/middlewares/responseHandler');

const FIRST_HORSE_ID = 1;
const horsesArray = [
  { id: 1, horseName: 'Red Rum', ownerName: 'Noel Le Mare', handicap: '11-08' },
  { id: 2, horseName: 'Lucius', ownerName: 'Fiona Whitaker', handicap: '10-09' }
];
const newHorseInput = { id: 3, horseName: 'Rubstic', ownerName: 'John Douglas', handicap: '10-00' };

let ctx;
let mockHorse = {};
const mockLokiProvider = { getCollections: () => ({ Horse: mockHorse }) };

describe('Given horses', () => {
  beforeEach(() => {
    ctx = { request: { body: {} }, params: {}, res: {} };
    responseHandler()(ctx, jest.fn());
    horseController.__Rewire__('lokiProvider', mockLokiProvider);
  });

  describe('When getting all horses', () => {
    it('<200> should return all horses in database', async () => {
      mockHorse.find = () => (horsesArray);
      await horseController.getHorses(ctx);
      expect(ctx.status).toEqual(200);
      expect(ctx.body.data).toEqual(horsesArray);
    });
  });

  describe('When finding a horse by id', () => {
    it('<200> should return matching horse', async () => {
      const firstHorse = horsesArray[0];
      mockHorse.findOne = () => (firstHorse);
      ctx.params.id = `${FIRST_HORSE_ID}`;
      await horseController.getHorse(ctx);
      expect(ctx.status).toEqual(200);
      expect(ctx.body.data).toEqual(firstHorse);
    });
  });

  describe('When deleting a horse by id', () => {
    it('<200> should return success', async () => {
      mockHorse.chain = jest.fn(() => (
        { find: () => (
          { remove: () => ({ status: 'success' }) }
        ) }
      ));
      ctx.params.id = `${FIRST_HORSE_ID}`;
      await horseController.deleteHorse(ctx);
      expect(ctx.status).toEqual(200);
    });
  });
});

describe('Given no horses', () => {
  beforeEach(() => {
    ctx = { request: { body: {} }, params: {}, res: {} };
    responseHandler()(ctx, jest.fn());
    horseController.__Rewire__('lokiProvider', mockLokiProvider);
  });

  afterEach(() => {
    __rewire_reset_all__();
  });

  describe('When getting all horses', () => {
    it('<200> should return an empty list', async () => {
      mockHorse.find = () => ([]);
      await horseController.getHorses(ctx);
      expect(ctx.status).toEqual(200);
      expect(ctx.body.data).toEqual([]);
    });
  });

  describe('When finding a horse by id', () => {
    it('<404> should return an error', async () => {
      mockHorse.findOne = () => (null);
      ctx.params.id = `${FIRST_HORSE_ID}`;
      await horseController.getHorse(ctx);
      expect(ctx.status).toEqual(404);
    });
  });

  describe('When creating a horse', () => {
    it('<200> should return the new record', async () => {
      mockHorse.insert = () => ({ ...newHorseInput, meta: {} });
      ctx.params.id = `${FIRST_HORSE_ID}`;
      await horseController.createHorse(ctx);
      expect(ctx.status).toEqual(201);
      expect(ctx.body.data).toEqual(expect.objectContaining(newHorseInput));
    });
  });

  describe('When deleting a horse by id', () => {
    it('<404> should return an error', async () => {
      mockHorse.chain = jest.fn();
      ctx.params.id = `${FIRST_HORSE_ID}`;
      await horseController.deleteHorse(ctx);
      expect(ctx.status).toEqual(400);
    });
  });
});

describe('Given error caused by bad data from client or from provider', () => {
  beforeEach(() => {
    ctx = { request: { body: {} }, params: {}, res: {} };
    responseHandler()(ctx, jest.fn());
    horseController.__Rewire__('lokiProvider', mockLokiProvider);
  });

  afterEach(() => {
    __rewire_reset_all__();
  });

  describe('When getting all horses', () => {
    it('<400> should return an error', async () => {
      mockHorse.find = () => { throw new Error(); };
      await horseController.getHorses(ctx);
      expect(ctx.status).toEqual(400);
    });
  });

  describe('When finding a horse by id', () => {
    it('<400> should return an error', async () => {
      mockHorse.findOne = () => { throw new Error(); };
      ctx.params.id = `${FIRST_HORSE_ID}`;
      await horseController.getHorse(ctx);
      expect(ctx.status).toEqual(400);
    });
  });

  describe('When creating a horse', () => {
    it('<200> should return the new record', async () => {
      mockHorse.insert = () => { throw new Error(); };
      ctx.params.id = `${FIRST_HORSE_ID}`;
      await horseController.createHorse(ctx);
      expect(ctx.status).toEqual(400);
    });
  });
});
