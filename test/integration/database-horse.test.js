'use strict';

const Loki = require('lokijs');
const supertest = require('supertest');
const app = require('../../app');
const testHelper = require('../testHelper');

const db = new Loki('loki.json', {
  autoload: true,
  autosave: true,
  autosaveInterval: 0
});
const Horse = db.addCollection('horse');

const horsesArray = [
  { id: 1, horseName: 'Red Rum', ownerName: 'Noel Le Mare', handicap: '11-08' },
  { id: 2, horseName: 'Lucius', ownerName: 'Fiona Whitaker', handicap: '10-09' }
];

const initialiseHorseDatabase = () => {
  Horse.clear();
};

const populateHorseDatabaseWithTestData = () => {
  horsesArray.forEach(function (horse) {
    // Destructure to stop the original object referenced being updated
    const { id, horseName, ownerName, handicap } = horse;
    Horse.insert({ id, horseName, ownerName, handicap });
  });
};

describe('Horses', () => {
  const request = supertest(app.listen());

  describe('Given horses in database', () => {
    beforeAll(() => {
      initialiseHorseDatabase();
      populateHorseDatabaseWithTestData();
      db.saveDatabase();
    });

    it('should return an entire list when Get Houses called', async () => {
      await testHelper.sleep(200);
      const res = await request
        .get('/horses')
        .expect('Content-Type', /json/)
        .expect(200);
      const actualArray = res.body.data;
      expect(actualArray).toHaveLength(2);
      expect(actualArray[0]).toEqual(expect.objectContaining(horsesArray[0]));
      expect(actualArray[1]).toEqual(expect.objectContaining(horsesArray[1]));
    });

    it('should return the matching horse when Get Houses called with id', async () => {
      await testHelper.sleep(200);
      const res = await request
        .get('/horses/1')
        .expect('Content-Type', /json/)
        .expect(200);
      const actualObject = res.body.data;
      expect(actualObject).toEqual(expect.objectContaining(horsesArray[0]));
    });
  });
});
