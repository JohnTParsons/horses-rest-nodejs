# Horses REST API


  A Node.js Koa RESTful API application with Docker, Swagger, Jest, Coveralls and Circle CI support

  By John Parsons

---

## Caveats

This is just a short-lived, theoretical example of a CRUD REST service based on the theme of horse racing.

NB It is currently a CRD service as I haven't had time to do the Update bit.

---

## Features

- Configuration for [Docker](https://www.docker.com/) based on Alpine (as that creates a small and optimized image)
- [Swagger](https://swagger.io/) API documentation based on JSDoc
- Continuous integration and delivery support, using [CircleCI](https://circleci.com/)
- Unit Test and Integration Test along with Test Coverage using [Jest](https://facebook.github.io/jest/) testing framework
- Logging
- Pre-commit hook
- Fast, lightweight in-memory database [LokiJS](http://lokijs.org/)
- Works with latest versions of Node and Yarn/NPM

---


## Getting Started

After downloading/cloning this repository:

```zsh
$ yarn
$ yarn start
```


## Commands

### Run

```zsh
# Run normally
$ yarn start
# Run the application with nodemon for development
$ yarn dev
```

### Test

```zsh
# Test
$ yarn test                           # Run all test
$ yarn test:unit                      # Run only unit test
$ yarn test:integration               # Run only integration test
# Test (Watch Mode for development)
$ yarn test:watch                     # Run all test with watch mode
$ yarn test:watch:unit                # Run only unit test with watch mode
$ yarn test:watch:integration         # Run only integration test with watch mode
# Test Coverage
$ yarn test:coverage                  # Calculate the coverage of all test
$ yarn test:coverage:unit             # Calculate the coverage of unit test
$ yarn test:coverage:integration      # Calculate the coverage of integration test
# Test consistent coding style (Lint)
$ yarn lint                           # Lint all sourcecode
$ yarn lint:app                       # Lint app sourcecode
$ yarn lint:test                      # Lint test sourcecode
```

### Archive

```zsh
$ yarn pack
```

---

#### RESULTS

### Manual testing results

HOME PAGE
\
curl http://localhost:7070

    {"status":"success","data":{"name":"horses-rest-nodejs","version":"1.0.0","description":"A Koa RESTful API application for Horses CRUD operations","author":{"name":"John Parsons"}},"message":"Hello, API!"}jpa33@OSTML0195840 horses-rest-node

SPEC PAGE
\
curl http://localhost:7070/spec

    {"info":{"title":"horses-rest-nodejs","description":"A Koa RESTful API application for Horses CRUD operations","version":"1.0.0","contact":{"name":"John Parsons"}},"consumes":["application/x-www-form-urlencoded","application/json"],"produces":["application/json"],"securityDefinitions":{"Authorization":{"in":"header","type":"apiKey","name":"Authorization","description":"The credentials to authenticate a user"}},"swagger":"2.0","paths":{"/":{"get":{"tags":["Public"],"summary":"Show API information.","operationId":"showApiInfo","responses":{"200":{"description":"Describe general API information"}}}},"/horses":{"get":{"tags":["Public"],"summary":"Get all horses","operationId":"getHorses","responses":{"200":{"description":"0-to-many horses found"},"400":{"description":"Error finding horses"}}}},"/horses/:id":{"get":{"tags":["Public"],"summary":"Find a horse","operationId":"getHorses","responses":{"200":{"description":"Matching horse found"},"400":{"description":"Error finding horses"},"404":{"description":"No matching horse found"}}}}},"definitions":{"ResponseStatuses":{"type":"string","enum":["success","fail","error"]},"ResponseFailCodes":{"type":"string","enum":["INVALID_REQUEST","UNKNOWN_ENDPOINT","INVALID_IMAGE_FORMAT","UNSUPPORTED_IMAGE_FORMAT","INVALID_IMAGE_DIMENSIONS","IMAGE_SIZE_LIMIT","UNKNOWN_DOMAIN_NAME"]},"ResponseErrorCodes":{"type":"string","enum":["INTERNAL_ERROR","UNKNOWN_ERROR"]},"SuccessResponse":{"type":"object","properties":{"status":{"$ref":"#/definitions/ResponseStatuses"},"data":{"type":"object"},"message":{"type":"string"}},"required":["status","data","message"]},"FailResponse":{"type":"object","properties":{"status":{"$ref":"#/definitions/ResponseStatuses"},"code":{"$ref":"#/definitions/ResponseFailCodes"},"data":{"type":"object"},"message":{"type":"string"}},"required":["status","code","message"],"example":{"status":"fail","code":"INVALID_IMAGE_FORMAT","data":null,"message":"This file is not a valid image."}},"ErrorResponse":{"type":"object","properties":{"status":{"$ref":"#/definitions/ResponseStatuses"},"code":{"$ref":"#/definitions/ResponseErrorCodes"},"data":{"type":"object"},"message":{"type":"string"}},"required":["status","code","message"],"example":{"status":"error","code":"UNKNOWN_ERROR","data":null,"message":"The server encountered an unknown error."}}},"responses":{"BadRequest":{"description":"BadRequest","schema":{"$ref":"#/definitions/FailResponse"},"examples":{"application/json":{"status":"fail","code":"INVALID_REQUEST","data":null,"message":"The request has invalid parameters."}},"headers":{"X-Request-Id":{"description":"The request id.","type":"string","format":"uuid"}}},"UnprocessableEntity":{"description":"UnprocessableEntity","schema":{"$ref":"#/definitions/FailResponse"},"examples":{"application/json":{"status":"fail","code":"IMAGE_SIZE_LIMIT","data":null,"message":"The size of this image is too large."}},"headers":{"X-Request-Id":{"description":"The request id.","type":"string","format":"uuid"}}},"InternalServerError":{"description":"InternalServerError","schema":{"$ref":"#/definitions/ErrorResponse"},"examples":{"application/json":{"status":"error","code":"INTERNAL_FAILURE","data":null,"message":"The server encountered an internal error."}},"headers":{"X-Request-Id":{"description":"The request id.","type":"string","format":"uuid"}}},"GatewayTimeOut":{"description":"GatewayTimeOut","schema":{"$ref":"#/definitions/ErrorResponse"},"examples":{"application/json":{"status":"error","code":"TRANSACTION_TIMEOUT","data":null,"message":"The request was timed out."}},"headers":{"X-Request-Id":{"description":"The request id.","type":"string","format":"uuid"}}}},"parameters":{"action":{"name":"action","in":"query","type":"string","required":true,"description":"Action parameter for non-CRUD actions"}},"tags":[{"name":"Public","description":"Public APIs"}]}

READ INITIAL CONTENT
\
curl http://localhost:7070/horses

    {"status":"success","data":[],"message":"getHorses success"}

CREATE A HORSE
\
curl -d '{ "id": 1, "horseName": "Red Rum", "ownerName": "Noel Le Mare", "handicap": "11-08" }' -H "Content-Type: application/json" -X POST http://localhost:7070/horses

    {"status":"success","data":{"id":1,"horseName":"Red Rum","ownerName":"Noel Le Mare","handicap":"11-08","meta":{"revision":0,"created":1525290169374,"version":0},"$loki":1},"message":"createHorse success"}

CREATE ANOTHER HORSE
\
curl -d '{ "id": 2, "horseName": "Lucius", "ownerName": "Fiona Whitaker", "handicap": "10-09" }' -H "Content-Type: application/json" -X POST http://localhost:7070/horses

    {"status":"success","data":{"id":2,"horseName":"Lucius","ownerName":"Fiona Whitaker","handicap":"10-09","meta":{"revision":0,"created":1525290550703,"version":0},"$loki":2},"message":"createHorse success"

READ ALL HORSES
\
curl http://localhost:7070/horses

    {"status":"success","data":[{"id":1,"horseName":"Red Rum","ownerName":"Noel Le Mare","handicap":"11-08","meta":{"revision":0,"created":1525290169374,"version":0},"$loki":1},{"id":2,"horseName":"Lucius","ownerName":"Fiona Whitaker","handicap":"10-09","meta":{"revision":0,"created":1525290550703,"version":0},"$loki":2}],"message":"getHorses success"}

READ A HORSE
\
curl http://localhost:7070/horses/2

    {"status":"success","data":{"id":2,"horseName":"Lucius","ownerName":"Fiona Whitaker","handicap":"10-09","meta":{"revision":0,"created":1525290550703,"version":0},"$loki":2},"message":"getHorse success"}

DELETE A HORSE
\
curl -H "Content-Type: application/json" -X DELETE http://localhost:7070/horses/1

    {"status":"success","data":{"collection":null,"filteredrows":[],"filterInitialized":true},"message":"deleteHorse success"}

READ ALL HORSES
\
curl http://localhost:7070/horses

    {"status":"success","data":[{"id":2,"horseName":"Lucius","ownerName":"Fiona Whitaker","handicap":"10-09","meta":{"revision":0,"created":1525290550703,"version":0},"$loki":2}],"message":"getHorses success"}

READ NON-EXISTENT HORSE
\
curl http://localhost:7070/horses/1

    {"status":"fail","code":null,"data":null,"message":"getHorse match failure"}


### Linting

There are no lint errors.

yarn lint

    yarn run v1.6.0
    $ eslint app test
    ✨  Done in 0.95s.


### Coverage

There is 100% coverage on the main files that I added: horse.js and testHelper.js. lokiProvider.js is the remaining file that I added and is missing coverage on only 1 line.

The remaining coverage issues aren't strictly my fault as they are on files I inherited from a github open source project called koa-rest-api-boilerplate, which I used as the starting point for this solution. I chose it as it came with default setup for Koa, Jest and Docker, all of which I wanted to use as part of the solution.

yarn test:coverage

File                 |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
---------------------|----------|----------|----------|----------|-------------------|
All files            |    80.62 |       50 |       60 |    80.53 |                   |
 app                 |     87.5 |       25 |        0 |     87.5 |                   |
  apm.js             |      100 |      100 |      100 |      100 |                   |
  index.js           |       80 |       25 |        0 |       80 | 50,51,52,60,61,63 |
  logger.js          |      100 |      100 |      100 |      100 |                   |
  routes.js          |      100 |      100 |      100 |      100 |                   |
 app/config          |    82.35 |    72.22 |      100 |    82.35 |                   |
  index.js           |      100 |     87.5 |      100 |      100 |                 9 |
  logger.js          |    72.73 |       60 |      100 |    72.73 |          17,24,30 |
 app/constants       |      100 |      100 |      100 |      100 |                   |
  error.js           |      100 |      100 |      100 |      100 |                   |
 app/controllers     |      100 |      100 |      100 |      100 |                   |
  home.js            |      100 |      100 |      100 |      100 |                   |
  horse.js           |      100 |      100 |      100 |      100 |                   |
 app/database        |       90 |       50 |      100 |    88.89 |                   |
  lokiProvider.js    |       90 |       50 |      100 |    88.89 |                11 |
 app/middlewares     |    64.58 |       40 |       50 |    64.58 |                   |
  errorHandler.js    |    66.67 |       40 |      100 |    66.67 |          19,21,24 |
  log.js             |    59.09 |    23.08 |       40 |    59.09 |... 22,26,47,66,70 |
  requestId.js       |      100 |      100 |      100 |      100 |                   |
  responseHandler.js |     61.4 |    31.25 |    42.11 |     61.4 |... 10,111,115,116 |
 app/spec            |      100 |      100 |      100 |      100 |                   |
  index.js           |      100 |      100 |      100 |      100 |                   |
 test                |      100 |      100 |      100 |      100 |                   |
  testHelper.js      |      100 |      100 |      100 |      100 |                   |

    Test Suites: 3 passed, 3 total
    Tests:       14 passed, 14 total
    Snapshots:   0 total
    Time:        1.947s
    Ran all test suites.
    ✨  Done in 2.82s.
